#!/bin/bash

# TRex testing script for Maestro
# Ilia Anokhin
# v1.22

trexdir=/opt/trex/v2.74
[[ $1 ]] && testname=$1 || testname=http
[[ $2 ]] && throughput=$2 || throughput=1
[[ $3 ]] && duration=$3 || duration=600

case $testname in
        imix)
        testfile="cap2/imix_1518.yaml"
        multiplier=$(expr "10.30 * $throughput" | bc)
        ;;

        http)
        testfile="cap2/http_simple.yaml"
        multiplier=$(expr "1303 * $throughput" | bc)
        ;;

        sfrmix)
        testfile="avl/sfr_delay_10_1g_no_bundeling.yaml"
        multiplier=$(expr "1.00002 * $throughput" | bc)
        ;;

        *)
        printf "Usage: `basename $0` [ imix | http | sfrmix ] [ throughput ] [ duration ]\n"
        exit 64
        ;;
esac

cd $trexdir
case $4 in
        loop)
        ./t-rex-64 --cfg ../chkp/dut-loop.yaml -f $testfile -c 1 -m $multiplier -d $duration
        exit 1
        ;;

        *)
        ./t-rex-64 --cfg ../chkp/dut-maestro.yaml -f $testfile -c 1 -m $multiplier -d $duration
        exit 0
        ;;
esac
